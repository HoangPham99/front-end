import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Interviewee } from '../models/interviewee.model';
import { IntervieweeService } from '../service/interviewee.service';

@Component({
  selector: 'app-interviewee-details',
  templateUrl: './interviewee-details.component.html',
  styleUrls: ['./interviewee-details.component.scss']
})
export class IntervieweeDetailsComponent implements OnInit {
  userID: any;
  itv: Interviewee;
  defaultAva = 'https://mdbootstrap.com/img/Photos/Avatars/img%20(27).jpg';
  techniques: Array<string>;
  constructor(private router: ActivatedRoute, private routerLink: Router,
    private intervieweeService: IntervieweeService) {
    this.userID = this.router.snapshot.params.id;
    console.log(this.userID);

  }

  ngOnInit() {
    this.intervieweeService.getIntervieweeByID(this.userID).subscribe(
      (res) => {
        this.itv = res;
        console.log(this.itv);
        this.techniques = this.itv.technique.split(',');
        this.techniques.forEach((item,index) => {
          this.techniques[index] = this.techniques[index].trim();
        });    
        console.log(this.techniques);
          
      },
      (err) => {
        console.log(err);
      }
    );
  }


  deleteInterviewee(iModal) {
    this.intervieweeService.deleteInterviewee(this.userID).subscribe(
      (success) => {
        iModal.hide();
        this.routerLink.navigateByUrl('dashboard');
      }
    )
  }

}

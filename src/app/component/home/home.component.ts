import { Component, OnInit } from '@angular/core';
import { Kobiton } from 'protractor/built/driverProviders';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}

import { Component, OnInit , AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service'
import { IntervieweeService } from '../../service/interviewee.service'
import {CookieService} from 'ngx-cookie-service'
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']

})
export class NavbarComponent implements OnInit,AfterViewInit {
  FilterActive :boolean = false;
  ngAfterViewInit(): void {
    document.getElementsByClassName
  }
  navOpen = false;
  state: String = 'inactive'
  fullname: string;
  constructor(private router: Router,
    private cookieService: CookieService,
    public authenticationService: AuthenticationService,
    private intervieweeService: IntervieweeService
  ) { }

  ngOnInit() {
    this.authenticationService.userCurrent().subscribe(
      (data) => {
        this.fullname = data.name
        localStorage.setItem('fullname', data.name);
      }
    )
  }

  logout() {
    this.authenticationService.logout().subscribe(
      (data) => {
        data
        this.router.navigateByUrl('login')
        localStorage.removeItem('token');
        localStorage.removeItem('fullname');
      }
    )
  }

  

  
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { IntervieweeService } from '../service/interviewee.service';
import { Interviewee } from '../models/interviewee.model';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { SearchComponent } from '../search/search.component';
import { ListIntervieweeComponent } from '../list-interviewee/list-interviewee.component';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(SearchComponent) private searchComponent: SearchComponent;
  @ViewChild(ListIntervieweeComponent) private listInterviewee: ListIntervieweeComponent;
  searchParam: any = '';
  statusSelected: Array<string> = [];
  posSelected: Array<string> = [];
  statusSelectedMB: Array<string> = [];
  posSelectedMB: Array<string> = [];
  dateFrom: string = '';
  dateTo: string = '';
  positionList: Array<any> = [];
  statusList = [];
  selectedItems = [];
  dropdownSettings = {};
  SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
  filterData: object;
  formFilter: FormGroup;
  constructor(private intervieweeService: IntervieweeService,
    private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    // retrive search param from search component
    this.route.queryParams.subscribe(params => {
      if (params.search) {
        this.searchParam = params.search;
        console.log('get param');    
      } else {
        this.searchParam = '';
        console.log('no param');
      }
    });
    //this.initData();
    this.initFilterData();
  }

  //----------------------------------- Initialize ---------------------------------------------
  async initFilterData() {
    this.statusList = [
      { item_id: 1, item_text: 'PASSED' },
      { item_id: 2, item_text: 'FAILED' },
      { item_id: 3, item_text: 'WAITING' },
      { item_id: 4, item_text: 'INCOMING' },
    ];
    await this.intervieweeService.getFilterList().subscribe(
      (res) => {
        res.position_list.forEach(element => {
          this.positionList.push({ item_id: element[0], item_text: element[1] });
        });
        this.initForm();
      }
    );
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      enableCheckAll: true,
      selectAllText: 'Check all',
      unSelectAllText: 'Uncheck all',
      closeDropDownOnSelection: false,
      itemsShowLimit: 2,
      maxHeight: 197,
      allowSearchFilter: false
    };
  }
  // form setup 
  initForm() {
    this.formFilter = this.formBuilder.group({
      posPreferences: this.addPosControl(),
      statusPreferences: this.addStatusControl(),
      dateRange: '',
      dateShow: [{ value: '', disabled: true }],
      mutilPos: '',
      mutilStatus: ''
    });
    this.onDateChanges();
  }
  addPosControl() {
    const arr = this.positionList.map(element => {
      return this.formBuilder.control(false);
    });
    return this.formBuilder.array(arr);
  }
  addStatusControl() {
    const arr = this.statusList.map(element => {
      return this.formBuilder.control(false);
    });
    return this.formBuilder.array(arr);
  }
  onDateChanges(): void {
    this.formFilter.get('dateRange').valueChanges.subscribe(val => {
      if (val) {
        this.frmDateShow.setValue(val);
        if (val.length > 10 && val !== 'undefined') {
          let date = val.split('to');
          this.dateFrom = this.formatDate(date[0]);
          this.dateTo = this.formatDate(date[1]);
        } else {
          this.dateFrom = this.formatDate(val);
        }
      }
    });
  }
  // -----------------------------------End Initialize --------------------------------------------
  //----------------------------------- form submit ---------------------------------------------

  /**
   * should set the array object to put inside the router
   * check is null or not define 
   * remove on url if it null or not define 
   * replace if that change 
   */
  doFilter() {
    console.log('PC');
    let status = (typeof this.statusSelected.length !== 'undefined') && (this.statusSelected.length > 0) ? { status: this.statusSelected } : null;
    let position = (typeof this.posSelected.length !== 'undefined') && (this.posSelected.length > 0) ? { position: this.posSelected } : null;
    let dateFrom = (this.dateFrom.length > 0) ? { from: this.dateFrom } : null;
    let dateTo = (this.dateTo.length > 0) ? { to: this.dateTo } : null;

    console.log(status + ' and ' + position);
    let _queryParam: any;
    // retrive all data 
    _queryParam = status || position || dateFrom || dateTo ? { ...status, ...position, ...dateFrom, ...dateTo } :
      { status: null, position: null, dateFrom: null, dateTo: null };

    // some operator to remove null 
    _queryParam = !status ? { ..._queryParam, ...{ status: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !position ? { ..._queryParam, ...{ position: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !dateFrom ? { ..._queryParam, ...{ from: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !dateTo ? { ..._queryParam, ...{ to: null } } : { ...status, ...position, ...dateFrom, ...dateTo };

    console.log('query ' + JSON.stringify(_queryParam));

    if (this.searchParam) {
      console.log('got a param :' + this.searchParam);
      _queryParam = { ..._queryParam, ... { search: this.searchParam } };
      console.log('query inside' + JSON.stringify(_queryParam));
      this.router.navigate([], { queryParams: _queryParam });
    } else {
      console.log('No more param goted');
      this.router.navigate([], { queryParams: _queryParam });
    }
  }

  doFilterMB() {
    console.log('MB');
    let status = (typeof this.statusSelectedMB.length !== 'undefined') && (this.statusSelectedMB.length > 0) ? { status: this.statusSelectedMB } : null;
    let position = (typeof this.posSelectedMB.length !== 'undefined') && (this.posSelectedMB.length > 0) ? { position: this.posSelectedMB } : null;
    let dateFrom = (this.dateFrom.length > 0) ? { from: this.dateFrom } : null;
    let dateTo = (this.dateTo.length > 0) ? { to: this.dateTo } : null;

    console.log(status + ' and ' + position);
    let _queryParam: any;
    // retrive all data 
    _queryParam = status || position || dateFrom || dateTo ? { ...status, ...position, ...dateFrom, ...dateTo } :
      { status: null, position: null, dateFrom: null, dateTo: null };


    // some operator to remove null value 
    _queryParam = !status ? { ..._queryParam, ...{ status: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !position ? { ..._queryParam, ...{ position: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !dateFrom ? { ..._queryParam, ...{ from: null } } : { ...status, ...position, ...dateFrom, ...dateTo };
    _queryParam = !dateTo ? { ..._queryParam, ...{ to: null } } : { ...status, ...position, ...dateFrom, ...dateTo };

    console.log('query ' + JSON.stringify(_queryParam));

    if (this.searchParam) {
      console.log('got a param :' + this.searchParam);
      _queryParam = { ..._queryParam, ... { search: this.searchParam } };
      console.log('query inside' + JSON.stringify(_queryParam));
      this.router.navigate([], { queryParams: _queryParam });
    } else {
      console.log('No more param goted');
      this.searchParam  = '';
      this.router.navigate([], { queryParams: _queryParam });
    }
  }
  // ---------------------------------- End form submit ---------------------------------------------------
  // ---------------------------------- Mutil select dropdown call-back event -----------------------------
  onItemPosSelect(item: any) {
    this.posSelectedMB.push(item.item_id);
    if (this.posSelectedMB === []) {
      this.posSelectedMB = null;
    }
  }
  onItemStatusSelect(item: any) {
    this.statusSelectedMB.push(item.item_text);
  }

  onSelectAll(items: any) {
    console.log('select all ' + items);
  }
  onItemPosDeSelect(items: any) {
    this.posSelectedMB = this.posSelectedMB.filter(e => e !== items.item_id);
    console.log(this.posSelectedMB);
  }
  onItemStatusDeSelect(items: any) {
    this.statusSelectedMB = this.statusSelectedMB.filter(e => e !== items.item_text);
  }
  // ----------------------------------End Mutil select dropdown -----------------------------

  //------------------------------------- Util ------------------------------------------------
  // swipe right actions
  swipe(action: string = this.SWIPE_ACTION.RIGHT) {
    console.log(action);
    if (action === this.SWIPE_ACTION.RIGHT) {
    }
  }

  formatDate(date) {
    return date.split("-").reverse().join("/").trim().replace(' ', '');
  }

  removeDateValue() {
    this.dateFrom = '';
    this.dateTo = '';
    this.frmDateShow.reset();
  }


  //----------------------------------- Checkbox event  ---------------------------------------------
  // get Select pos -> provide on change event of checkbox
  getSelectedPos() {
    this.posSelected = [];
    this.posArray.controls.forEach((item, i) => {
      if (item.value) {
        this.posSelected.push(this.positionList[i].item_id);
      }
    });
  }

  // get Select status -> provide on change event of checkbox
  getSelectedStatus() {
    this.statusSelected = [];
    this.statusArray.controls.forEach((item, i) => {
      if (item.value) {
        this.statusSelected.push(this.statusList[i].item_text);
      }
    });
  }

  public get posArray() {
    return <FormArray>this.formFilter.get('posPreferences');
  }

  public get statusArray() {
    return <FormArray>this.formFilter.get('statusPreferences');
  }

  public get frmDate() {
    return this.formFilter.controls.dateRange;
  }
  public get frmDateShow() {
    return this.formFilter.controls.dateShow;
  }
  //-----------------------------------End Checkbox event ---------------------------------------------

}

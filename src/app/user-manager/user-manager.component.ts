import { Component, OnInit, ViewChild, ChangeDetectorRef, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from "@angular/forms";
import { UserService } from '../service/user.service';
import { User } from '../models/user.model'



@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.scss']
})
export class UserManagerComponent implements OnInit {
  @Input() userDetails: User = { id: '', email: '', password: '', nickname: '', role: '' }

  loginFormModalUsername = new FormControl('', Validators.email);
  loginFormModalPassword = new FormControl('', Validators.required);
  users: User[] = [];
  usersTemp: User[] = [];
  previous: any = [];
  message: string;
  errorEmailMsg: string;
  errorNicknameMsg: string;
  errorRoleMsg: string;
  errorPasswordMsg: string;
  headElements = ['ID', 'Email', 'Fullname', 'Role', 'Action'];
  config = {
    id: 'list_user',
    itemsPerPage: 5,
    currentPage: 1,
    totalItems: 0
  };
  formEdit: FormGroup;
  formAdd: FormGroup;
  idUser: any;
  constructor(public userService: UserService, private fb: FormBuilder) {

  }

  ngOnInit() {
    this.loadUser();

    this.formEdit = this.fb.group({
      id: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ['', Validators.required],
      nickname: ['', Validators.required],
      role: ['', Validators.required]
    });

    this.formAdd = this.fb.group({
      id: [''],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ['', Validators.required],
      nickname: ['', Validators.required],
      role: ['', Validators.required]
    });


  }


  /** get All user */
  loadUser() {
    this.userService.getUser().subscribe(data => {
      this.users = data;
      this.usersTemp = data;
    });
  }



  // edit user
  ID: number;
  edit(iModal, id, email, password, nickname, role): void {
    iModal.show();
    this.initDefaultErrMsg();
    this.formEdit = this.fb.group({
      id: [id],
      email: [email, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: [password, Validators.required],
      nickname: [nickname, Validators.required],
      role: [role, Validators.required]
    });
  }
  // get user id to delete
  deleteUser(iModal, id) {
    iModal.show();
    this.idUser = id;

  }
  deleteOnServer(iModal) {
    this.userService.deleteUser(this.idUser).subscribe(data => {
      this.users = this.users.filter(e => e.id !== this.idUser);
    });
    iModal.hide();
  }

  addUser(iModal) {
    if (this.validForm(this.formAdd)) {
      this.userService.createUser(this.formAdd.value).subscribe(
        res => {
          this.formAdd.value.id = this.users[this.users.length - 1].id + 1;
          this.users.push(this.formAdd.value);
        }
      );
      iModal.hide();
    } else {
      return;
    }
  }

  /**
   * Edit user and replace that if edit success 
   * @param iModal : Edit modal that need to hide after we edit 
   */
  editUser(iModal) {
    this.initDefaultErrMsg();
    if (this.validForm(this.formEdit)) {
      this.userService.updateUser(this.getFormValue.id, this.getFormValue).subscribe(
        res => {
          this.users.forEach((item, i) => {
            if (item.id === res.id) {
              this.users[i] = res;
            }
          });
        });

      iModal.hide();
    } else {
      return;
    }


  }


  // Off line search because list user is short , we just got all and search
  doSearch(event) {

    let searchStr = event.target.value;
    if (!searchStr.trim()) {
      this.users = this.usersTemp;
      this.message = null;
      return;
    }
    let listUsersSearched: User[] = [];
    this.users.forEach((item, i) => {
      if (item.nickname.includes(searchStr) ||
        item.email.includes(searchStr) ||
        item.role.includes(searchStr)) {
        listUsersSearched.push(item);
      }
    });

    if (listUsersSearched.length !== 0) {
      this.users = listUsersSearched;
      this.message = listUsersSearched.length + ' User found';
    } else {
      this.message = listUsersSearched.length + ' User found - Show all user';
    }
  }



  // nested form value
  public get getFormValue(): any {
    return this.formEdit.value;
  }


  // Event for ngx-pagination
  pageChange(event) {
    this.config.currentPage = event;
  }


  // Validate 2 form 
  private validForm(form: FormGroup): boolean {
    if (!form.valid) {
      if (form.get('email').hasError('required')) {
        this.errorEmailMsg = 'Email is required';
      } else if (form.get('email').hasError('email')) {
        this.errorEmailMsg = 'Email is invalid';
      } else {
        this.errorEmailMsg = null;
      }

      if (form.get('password').hasError('required')) {
        this.errorPasswordMsg = 'Password is required';
      } else {
        this.errorPasswordMsg = null;
      }
      if (form.get('nickname').hasError('required')) {
        this.errorNicknameMsg = 'nickname is required';
      } else {
        this.errorNicknameMsg = null;
      }
      if (form.get('role').hasError('required')) {
        this.errorRoleMsg = 'Role is required';
      } else {
        this.errorRoleMsg = null;
      }
      return false;
    }
    return true;
  }

  initDefaultErrMsg(){
    this.errorEmailMsg = null;
    this.errorNicknameMsg = null;
    this.errorPasswordMsg = null; 
    this.errorRoleMsg = null;
  }
}

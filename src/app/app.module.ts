import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MDBBootstrapModule, DropdownModule } from 'angular-bootstrap-md';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { FlatpickrModule } from 'angularx-flatpickr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { LoginComponent } from './component/login/login.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { SearchComponent } from './search/search.component';
import { ListIntervieweeComponent } from './list-interviewee/list-interviewee.component'
import { IntervieweeDetailsComponent } from './interviewee-details/interviewee-details.component';
import { UpdateIntervieweeComponent } from './update-interviewee/update-interviewee.component';
import { ListGhostComponent } from './list-interviewee/list-ghost/list-ghost.component';
import { TagsInputComponent } from './util/tags-input/tags-input.component';


import { AuthGuard } from './service/auth.guard'
import { AuthenticationService } from './service/authentication.service';
import { CookieService } from 'ngx-cookie-service';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { ToastrModule } from 'ngx-toastr';
import { AuthInterceptor } from './service/auth.interceptor';
import { ErrorInterceptor } from './service/error.interceptor';


export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': { velocity: 0.4, threshold: 20 } // override default settings
  }
}


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    SidebarComponent,
    LayoutComponent,
    DashboardComponent,
    UserManagerComponent,
    LoginComponent,
    PageNotFoundComponent,
    SearchComponent,
    IntervieweeDetailsComponent,
    ListIntervieweeComponent,
    UpdateIntervieweeComponent,
    ImageUploadComponent,
    ListGhostComponent,
    TagsInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    DropdownModule.forRoot(),
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgMultiSelectDropDownModule.forRoot(),
    FlatpickrModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      closeButton: true,
      enableHtml: true,
      progressBar: true,
      progressAnimation: "increasing",
      timeOut: 2500,
    })

  ],
  providers: [
    AuthGuard, AuthenticationService, CookieService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { User } from '../models/user.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { retry, catchError, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly apiURL = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }
  counterUrl = 'http://localhost:3000/user_count';
  getUserCount(): Observable<any> {
    return this.http.get(this.counterUrl).pipe(tap(res => console.log(res)));
  }


  //GET method => fetch user data
  getUser(): Observable<User[]> {
    return this.http.get<User[]>(this.apiURL + '/user').pipe(
      tap(res => console.log(res)),
      catchError(error => of(null)),
    );
  }
  //DELETE method => delete user
  deleteUser(id): Observable<any> {
    return this.http.delete(this.apiURL + "/user/" + id).pipe(
      tap(res => console.log(res)),
      catchError(error => of(null)),

    );
  }
  //POST method => create user
  createUser(user): Observable<User> {
    return this.http.post<User>(this.apiURL + '/user', JSON.stringify(user), this.httpOptions).pipe(retry(1))
  }
  //PUT method => update user
  updateUser(id, user): Observable<User> {
    return this.http.put<User>(this.apiURL + '/user/' + id, JSON.stringify(user), this.httpOptions).pipe(
      tap(res => console.log(res),
        catchError(error => of(new User))
      ));
  }

  // Search with param nickname  

  searchByUserName(name): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiURL}/user?nickname_like=${name}`).pipe(
      tap(res => console.log(res)),
      catchError(err => of([]))
    )
  }

  // Search with param nickname  
  searchByserEmail(email): Observable<User[]> {
    return this.http.get<User[]>(`${this.apiURL}/user?email_like=${email}`).pipe(
      tap(res => console.log(res)),
      catchError(err => of([]))
    )
  }
}

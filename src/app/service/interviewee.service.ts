import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { tap, catchError } from 'rxjs/operators';
import { Interviewee } from '../models/interviewee.model';


@Injectable({
  providedIn: 'root'
})
export class IntervieweeService {
  readonly baseUrl = 'http://192.168.78.114:9999/api';
  constructor(private http: HttpClient) { }

  // TEST 
  getAllInterviewee(): Observable<any> {
    return this.http.get(`${this.baseUrl}/interviewee/search`).pipe(
      tap(res => {
        res
      }),
      catchError(error => of([]))
    );
  }
  // Get user by id
  getIntervieweeByID(id): Observable<Interviewee> {
    return this.http.get<Interviewee>(`${this.baseUrl}/interviewee/${id}`).pipe(
      tap(res => res),
      catchError(err => of(err))
    );
  }

  /** get list interviewee pagination 
  @param page : page number
  @param limit : number of record want to show 
  */
  getInterviewee(page: number, limit: number): Observable<any> {

    return this.http.get(`${this.baseUrl}/interviewee/search?page=${page}&size=${limit}`).pipe(
      tap(res => res),
      catchError(error => of(null))
    );
    
  }
  /**drop intervieweee */
  deleteInterviewee(id) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  /** Search by username 
  @param name : username want to search 
  */
  searchByName(page: number, limit: number, name: string): Observable<any> {
    // if name is undefine
    if (!name.trim()) {
      return of([]);
    }
    const url = `${this.baseUrl}/interviewee/search?name=${name}&page=${page}&size=${limit}`;
    return this.http.get(url).pipe(
      tap(res => {
        console.log(url);

        console.log(JSON.stringify(res));
      }),
      catchError(error => of([]))
    );
  }

  /** Search with param (filter)
    @param fullname
   @param season
   @param type
   @param position
   @param from
   @param to
  */
  searchAllParam(name: string,
    position: string, status: string, from: string, to: string, page: number, limit: number): Observable<any> {

    const searchParamUrl = `${this.baseUrl}/interviewee/search?name=${name}&positionId=${position}&status=${status}&from=${from}&to=${to}&page=${page}&size=${limit}`;
    console.log(searchParamUrl);
    return this.http.get(searchParamUrl).pipe(
      tap(res => {
        console.log(res);
      }),
      catchError(error => of([]))
    );
  }


  //-------------------------------------------------Get filter param --------------------------------------------------


  getFilterList(): Observable<any> {
    return this.http.get('http://192.168.78.114:9999/api/filterComponent').pipe(
      tap(res => {
        console.log(res);
      }),
      catchError(error => of(error))
    );
  }


}

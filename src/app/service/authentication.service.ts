import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model'
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { CookieService } from 'ngx-cookie-service'
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private cookieService: CookieService,private http: HttpClient) { }
  readonly baseUrl = "http://192.168.78.114:9999";
  logout(): Observable<any> {
    return this.http.get(this.baseUrl + '/api/user/logout')
      .pipe(tap(
        (res) => {
          res
        }
      ),
        catchError(err => of(err))
      )
  }
  login(username: string, password: string): Observable<any> {
    return this.http.post<any>(this.baseUrl + '/api/user/login', { "username": username, "password": password })
      .pipe(tap(
        (res) => {
        }
      ))
  }
  userCurrent(): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/api/user/current')
      .pipe(tap(
        (res) => {
          res
        }
      ), catchError(err => of(err)

      ))
  }
}

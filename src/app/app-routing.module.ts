import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { LayoutComponent } from './layout/layout/layout.component';
import { LoginComponent } from './component/login/login.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';

import { IntervieweeDetailsComponent } from './interviewee-details/interviewee-details.component';

import { AuthGuard } from './service/auth.guard'
import { UpdateIntervieweeComponent } from './update-interviewee/update-interviewee.component';

const routes: Routes = [
  {
    path: 'dashboard', component: LayoutComponent, children: [
      {
        path: '', component: DashboardComponent, canActivate: [AuthGuard]
      },
      {
        path: 'user', component: UserManagerComponent, canActivate: [AuthGuard]
      },
      {
        path: "interviewee/:id", component: IntervieweeDetailsComponent, canActivate: [AuthGuard]
      },
      {
        path: "update/:id", component: UpdateIntervieweeComponent, canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: '', redirectTo: 'dashboard', pathMatch: 'full', canActivate: [AuthGuard]
  },
  {
    path: '**', component: PageNotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }

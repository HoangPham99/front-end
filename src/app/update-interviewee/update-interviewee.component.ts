import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { IntervieweeService } from '../service/interviewee.service';
import { RouterLinkActive, ActivatedRoute } from '@angular/router';
import { Interviewee } from '../models/interviewee.model';
import { ImageUploadComponent } from '../image-upload/image-upload.component';

@Component({
  selector: 'app-update-interviewee',
  templateUrl: './update-interviewee.component.html',
  styleUrls: ['./update-interviewee.component.scss']
})




export class UpdateIntervieweeComponent implements OnInit {
  
  seasons = ['Summer-intern', 'Talent-class', 'Full-time'];
  techniques = ['JAVA', 'HTML5', 'CSS/CSS3', 'UI/UX' ,'ANDROID', 'REACT'];
  techniquesSelected;
  postions = ['Intern', 'Fresher', 'Senior', 'AI', 'FE', 'BE'];
  @ViewChild(ImageUploadComponent) imageUpload: ImageUploadComponent;
  interviewees: Interviewee;
  userID: any;
  date_schedule : any; 
  time_schedule : any;

  constructor(private formbuilder: FormBuilder,
    private intervieweeService: IntervieweeService,
    private router: ActivatedRoute) { }
  formUpdateInterview: FormGroup;


  ngOnInit() {
    this.userID = this.router.snapshot.params.id;
    // this.formUpdateInterview = this.formbuilder.group({
    //   status: ['', Validators.required],
    //   fullname: ['', Validators.required],
    //   attachCV: [''],
    //   gender: ['male'],
    //   address: [''],
    //   phoneNumber: [''],
    //   email: ['', Validators.compose([
    //     Validators.required,
    //     Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+.$')
    //   ])],
    //   room: ['', Validators.required],
    //   position: ['null'],
    //   note: ['']
    // });

    this.intervieweeService.getIntervieweeByID(this.userID).subscribe((res) => {
      this.interviewees = res;
      this.imageUpload.imageSrc = res.avatar;
      this.techniquesSelected = res.technique;
      this.date_schedule = res.time_interview.split(" ")[0];
      this.time_schedule = res.time_interview.split(" ")[1];
      
      
      this.formUpdateInterview = this.formbuilder.group({
        status: [res.status, Validators.required],
        fullname: [res.fullname, Validators.required],
        attachCV: [res.cv],
        gender: 'male',
        address: [res.address],
        birthday : [res.birthday],
        date_schedule :[this.date_schedule],
        time_schedule: [this.time_schedule],
        phoneNumber: [res.phone_number],
        email: [res.email, Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+.$')
        ])],
        room: [res.place_interview, Validators.required],
        position: [res.position],
        note: [res.note],
        techniques: [res.technique],
      });
    });


    // this.formUpdateInterview.valueChanges.subscribe((data) => {
    //   console.log(data);
    // });


  }

  saveInterviewee(form, iModal) {
    console.log(form);

    iModal.show();

  }
  deleteInterviewee(iModal) {
    iModal.show();
  }

  onSubmit() {

  }


  account_validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'status': [
      { type: 'required', message: 'Status is required' }
    ],
    'fullname': [
      { type: 'required', message: 'Fullname is required' }
    ],
    'dob': [
      { type: 'required', message: 'Dob is required' }
    ],
    'room': [
      { type: 'required', message: 'Room is required' }
    ]
  }

  formatDate(date) {
   return date.split("/").reverse().join("-");
  }


}
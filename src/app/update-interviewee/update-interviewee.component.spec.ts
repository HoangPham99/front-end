import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIntervieweeComponent } from './update-interviewee.component';

describe('UpdateIntervieweeComponent', () => {
  let component: UpdateIntervieweeComponent;
  let fixture: ComponentFixture<UpdateIntervieweeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateIntervieweeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIntervieweeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

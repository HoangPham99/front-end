export class User {
    id: string;
    email: string;
    password: string;
    nickname: string;
    role: string;
}
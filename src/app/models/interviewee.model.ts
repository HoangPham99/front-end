export class Interviewee {
    id: number;
    fullname: string;
    birthday : string;
    gender: boolean;
    email: string;
    address: string;
    phone_number: number;
    note: string;
    status: string;
    cv: string;
    avatar: string;
    technique: string;
    season_name: string;
    place_interview : string;
    position: string;
    time_interview: string;
    user : Array<any> ;
}